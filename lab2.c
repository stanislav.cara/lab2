#include <stdio.h>
#include <string.h>


typedef struct {
	char nr_elevi[15][100];
	int size;
}Laborator;

void afiseaza(Laborator* lab) {
	if (lab->size == 0) {
		printf("Nu exista studenti alocati in laborator.\n");
		return;
	}

	printf("Studentii alocati in laborator: ");
	for (int i = 0; i < lab->size; i++) {
		printf("%s", lab->nr_elevi[i]);
		if (i < lab->size - 1) {
			printf(", ");
		}
	}
	printf(".\n");
}

void elimina(Laborator *lab,const char *nume)
{
	for (int i = 0; i < lab->size; i++)
	{
		if (strcmp(lab -> nr_elevi[i], nume) == 0)
		{
			for (int j = 0; j < lab->size - 1; j++)
			{
				strcpy(lab->nr_elevi[j], lab->nr_elevi[j + 1]);
			}
			lab->size--;
			printf("Studentul %s a fost eliminat\n", nume);
			return;
		}
	}
	printf("Studentul nu a fost gasit\n");
}

int apartine(Laborator *lab,const char *nume)
{
	for (int i = 0; i < lab->size; i++)
	{
		if (strcmp(lab->nr_elevi[i], nume) == 0)
		{
			return 1; //Studentul exista
		}
	}
	return 0; //Studentul nu exista
}

void adauga(Laborator *lab,const char *nume)
{
	if (lab->size >= 15)
	{
		printf("Laboratorul este plin\n");
		return;
	}

	if (apartine(lab, nume))
	{
		printf("Studentul %s este deja alocat in laborator\n", nume);
		return;
	}
	strcpy(lab->nr_elevi[lab->size], nume);
	lab->size++;
	printf("Studentul %s a fost adaugat cu succes\n", nume);
}

int main()
{
	Laborator lab;
	lab.size = 0;

	adauga(&lab, "Eugeniu Caranic");
	adauga(&lab, "Dorin Bratu");
	adauga(&lab, "Stanislav Cara");

	afiseaza(&lab);

	elimina(&lab, "Stanislav Cara");
	afiseaza(&lab);

	if (apartine(&lab, "Andrei Popescu")) {
		printf("Andrei Popescu este deja alocat in laborator.\n");
	}
	else {
		printf("Andrei Georgescu nu este in laborator.\n");
	}

	afiseaza(&lab);


	return 0;
}